#include "TF2.h"
#include "Math/WrappedMultiTF1.h"
#include "Math/AdaptiveIntegratorMultiDim.h"
#include "Math/GSLMCIntegrator.h"

Double_t getCorrectionFactor(Double_t DUTMinX, Double_t DUTMinY,
                             Double_t DUTMaxX, Double_t DUTMaxY,
                             Double_t binMinX, Double_t binMinY,
                             Double_t binMaxX, Double_t binMaxY,
                             Double_t muX,     Double_t muY,
                             Double_t sigmaX,  Double_t sigmaY){
  TF2 g2("g2","[0]*TMath::Gaus(x,[1],[2])*TMath::Gaus(y,[3],[4])",DUTMinX,DUTMaxX,DUTMinY,DUTMaxY);
  ROOT::Math::WrappedMultiTF1 wf1(g2);
  g2.SetParameters(1.0/(2*3.141592654*sigmaX*sigmaY),muX,sigmaX,muY,sigmaY);
  ROOT::Math::AdaptiveIntegratorMultiDim ig;
  ig.SetFunction(wf1);
  ig.SetRelTolerance(1e-6);
  double rangeMinBin[2];
  rangeMinBin[0] = binMinX;
  rangeMinBin[1] = binMinY;
  double rangeMaxBin[2];
  rangeMaxBin[0] = binMaxX;
  rangeMaxBin[1] = binMaxY;
  Double32_t binVal = ig.Integral(rangeMinBin,rangeMaxBin);
  double rangeMinDUT[2];
  rangeMinDUT[0] = DUTMinX;
  rangeMinDUT[1] = DUTMinY;
  double rangeMaxDUT[2];
  rangeMaxDUT[0] = DUTMaxX;
  rangeMaxDUT[1] = DUTMaxY;
  Double_t norm = ig.Integral(rangeMinDUT,rangeMaxDUT);
  return binVal/std::pow(norm,2);
}

std::pair<Double_t,Double_t> GetDUTPosition(std::istream & input, Int_t eventID){
  std::string line;
  Int_t runID, minEventID, maxEventID;
  Double_t xPosDUT, yPosDUT;
  input.clear();
  input.seekg(0, ios::beg);
  while(std::getline(input,line)){
    if(line[0] == '#'){continue;}
    sscanf(line.c_str(),"%i %i %i %lf %lf",&runID,&minEventID,&maxEventID,&xPosDUT,&yPosDUT);
    if(eventID>minEventID && eventID <= maxEventID){break;}
  }
  std::pair<Double_t,Double_t> position;
  position.first = xPosDUT;
  position.second = yPosDUT;

  return position;
}

Double_t getCorrectionFactorError(Double_t DUTMinX, Double_t DUTMinY,
                                  Double_t DUTMaxX, Double_t DUTMaxY,
                                  Int_t resolutionX, Int_t resolutionY,
                                  Int_t xBin, Int_t yBin,
                                  Double_t muX, Double_t muY,
                                  Double_t sigmaX, Double_t sigmaY){
  Double_t stepSizeX = (DUTMaxX-DUTMinX)/resolutionX;
  Double_t stepSizeY = (DUTMaxY-DUTMinY)/resolutionY;
  TF2 g2D1X("g2D1X","((x-[1])**2/[2]**3-1/[2])*([0]*TMath::Gaus(x,[1],[2])*TMath::Gaus(y,[3],[4]))",DUTMinX,DUTMaxX,DUTMinY,DUTMaxY);
  TF2 g2D1Y("g2D1Y","((y-[3])**2/[4]**3-1/[4])*([0]*TMath::Gaus(x,[1],[2])*TMath::Gaus(y,[3],[4]))",DUTMinX,DUTMaxX,DUTMinY,DUTMaxY);
  g2D1X.SetParameters(1.0/(2*3.141592654*sigmaX*sigmaY),DUTMinX+xBin*stepSizeX+stepSizeX/2.0,sigmaX,DUTMinY+yBin*stepSizeY+stepSizeY/2.0,sigmaY);
  g2D1Y.SetParameters(1.0/(2*3.141592654*sigmaX*sigmaY),DUTMinX+xBin*stepSizeX+stepSizeX/2.0,sigmaX,DUTMinY+yBin*stepSizeY+stepSizeY/2.0,sigmaY);
  ROOT::Math::AdaptiveIntegratorMultiDim ig;
  ROOT::Math::WrappedMultiTF1 wf1Dx(g2D1X);
  ROOT::Math::WrappedMultiTF1 wf1Dy(g2D1Y);
  double rangeMin[2];
  rangeMin[0] = DUTMinX;
  rangeMin[1] = DUTMinY;
  double rangeMax[2];
  rangeMax[0] = DUTMaxX;
  rangeMax[1] = DUTMaxY;
  ig.SetFunction(wf1Dx);
  ig.SetRelTolerance(0.0001);
  Double32_t intValD1X = ig.Integral(rangeMin,rangeMax);
  ig.SetFunction(wf1Dy);
  ig.SetRelTolerance(0.0001);
  Double32_t intValD1Y = ig.Integral(rangeMin,rangeMax);

  return std::sqrt(std::pow(intValD1X*sigmaX,2)+std::pow(intValD1Y*sigmaY,2));
}

Double_t integrateGaus1D(Double_t min, Double_t max,
                         Double_t mu,  Double_t sigma){
  if(mu == min || mu == max){
    return 0.5;
  }
  if((mu-min) > 10*sigma && (max-mu) > 10*sigma){
    return 1.0;
  }
  TF1 g1("g1","[0]*TMath::Gaus(x,[1],[2])",min,max);
  g1.SetParameters(1.0/(std::sqrt(2*3.141592654)*sigma),mu,sigma);
  Double_t val = g1.Integral(min,max);
  return val;
}

Double_t integrateGaus1D_D1sigma(Double_t min, Double_t max,
                                 Double_t mu,  Double_t sigma){
  if((mu-min) > 10*sigma && (max-mu) > 10*sigma){
    return 0.0;
  }
  else{
    TF1 g1DS_test("g1DS_test","((x-[1])**2/[2]**3-1/[2])*[0]*TMath::Gaus(x,[1],[2])",min,max);
    g1DS_test.SetParameters(1.0/(std::sqrt(2*3.141592654)*sigma),mu,sigma);
    Double_t val = g1DS_test.Integral(min,max);
    return val;
  }
}

Double_t integrateGaus2D(Double_t minX,   Double_t maxX,
                         Double_t minY,   Double_t maxY,
                         Double_t muX,    Double_t muY,
                         Double_t sigmaX, Double_t sigmaY){
  TF2 g2("g2","[0]*TMath::Gaus(x,[1],[2])*TMath::Gaus(y,[3],[4])",minX,maxX,minY,maxY);
  g2.SetParameters(1.0/(2*3.141592654*sigmaX*sigmaY),muX,sigmaX,muY,sigmaY);
  Double_t val = g2.Integral(minX,maxX,minY,maxY);
  return val;
}

std::vector<Double_t> AmpToEnergy(Double_t amplitude, Int_t sampleID, Int_t pixelID, Int_t irradLevel){
  Double_t offset = 0.0;
  Double_t offset_error = 0.0;
  Double_t slope = 0.0;
  Double_t slope_error = 0.0;
  if(sampleID == 106){
    if(irradLevel == 0){
      if(pixelID == 0){
        offset = 5308.14;
        slope = 154.127;
        offset_error = 472.913;
        slope_error = 40.212;
      }
      else if(pixelID == 1){
        offset = 5202.92;
        slope = 158.941;
        offset_error = 524.474;
        slope_error = 44.7061;
      }
      else if(pixelID == 2){
        offset = 4839.37;
        slope = 136.518;
        offset_error = 427.379;
        slope_error = 36.6015;
      }
      else if(pixelID == 3){
        offset = 5297.99;
        slope = 150.976;
        offset_error = 433.193;
        slope_error = 37.833;
      }
    }
    else if(irradLevel == 1){
      if(pixelID == 0){

      }
      else if(pixelID == 1){

      }
      else if(pixelID == 2){

      }
      else if(pixelID == 3){

      }
    }
    else if(irradLevel == 2){
      if(pixelID == 0){

      }
      else if(pixelID == 1){

      }
      else if(pixelID == 2){

      }
      else if(pixelID == 3){

      }
    }

  }
  else if(sampleID == 112){
    if(irradLevel == 0){
      if(pixelID == 0){
        offset = 244.568;
        slope = 126.096;
        offset_error = 131.604;
        slope_error = 6.88731;
      }
      else if(pixelID == 1){
        offset = 232.001;
        slope = 128.354;
        offset_error = 140.525;
        slope_error = 7.79889;
      }
      else if(pixelID == 2){
        offset = 258.771;
        slope = 113.641;
        offset_error = 128.239;
        slope_error = 6.77804;
      }
      else if(pixelID == 3){
        offset = 321.533;
        slope = 124.157;
        offset_error = 122.814;
        slope_error = 6.29774;
      }
    }
    else if(irradLevel == 1){
      if(pixelID == 0){

      }
      else if(pixelID == 1){

      }
      else if(pixelID == 2){

      }
      else if(pixelID == 3){

      }
    }
  }
  else if(sampleID == 129){
    if(irradLevel == 0){
      if(pixelID == 0){
        offset = 96.9225;
        slope = 92.6224;
        offset_error = 239.175;
        slope_error = 11.661;
      }
      else if(pixelID == 1){
        offset = 96.9225;
        slope = 92.6224;
        offset_error = 239.175;
        slope_error = 11.661;
      }
      else if(pixelID == 2){
        offset = 172.28;
        slope = 86.9746;
        offset_error = 239.965;
        slope_error = 11.4435;
      }
      else if(pixelID == 3){
        offset = 248.983;
        slope = 86.0463;
        offset_error = 195.787;
        slope_error = 9.12688;
      }
    }
    else if(irradLevel == 1){
      if(pixelID == 0){

      }
      else if(pixelID == 1){

      }
      else if(pixelID == 2){

      }
      else if(pixelID == 3){

      }
    }
  }
  else{
    std::cout << "Error in AmpToEnergy: Sample, Pixel or IrradLevel not found!" << std::endl;
    abort();
  }
  std::vector<Double_t> energy;
  energy.push_back(offset+slope*amplitude);
  energy.push_back(std::sqrt(std::pow(offset_error,2)+std::pow(slope_error*amplitude,2)));
  return energy;
}

/*
std::vector<Double_t> calcAsymErrors(Double_t hitCount, Double_t totalCount,
                                     Double_t hitCountSymError, Double_t totalCountSymError){
  Double_t hitCountError = std::sqrt(hitCount);
  Double_t totalCountError = std::sqrt(totalCount);

 return NULL;
}*/
