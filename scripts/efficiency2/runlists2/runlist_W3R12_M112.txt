#list of runs for Matrix 112, irradiated to 1e15
#columns: runID - DUTSize (2x pixel size) - showPlots (0/1) - nBins - statLimit - startEventID - endEventID (0 when all should be read) - sysErrorPos - sysErrorNeg - thresholdFactor
          541     80                        0                 24      100         0              0                                        0.009         0.004         1.0
          548     80                        0                 24      100         0              0                                        0.009         0.004         1.0
          556     80                        0                 24      100         0              0                                        0.009         0.004         1.0
          559     80                        0                 24      100         0              0                                        0.009         0.004         1.0
          561     80                        0                 24      100         0              0                                        0.009         0.004         1.0
          564     80                        0                 24      100         0              0                                        0.009         0.004         1.0
          567     80                        0                 24      100         0              0                                        0.009         0.004         1.0
          570     80                        0                 24      100         0              0                                        0.009         0.004         1.0
          580     80                        0                 24      100         0              0                                        0.009         0.004         1.0
          583     80                        0                 24      100         0              0                                        0.009         0.004         1.0
          596     80                        0                 24      100         0              0                                        0.009         0.004         1.0
          606     80                        0                 24      100         0              0                                        0.009         0.004         1.0
          609     80                        0                 24      100         0              0                                        0.009         0.004         1.0
