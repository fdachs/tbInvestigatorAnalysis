#list of runs for Matrix 106, irradiated to 1e15
#columns: runID - DUTSize (2x pixel size) - showPlots (0/1) - nBins - statLimit - startEventID - endEventID (0 when all should be read) - sysErrorPos - sysErrorNeg - thresholdFactor
          705     60                        0                 18      100         0              0                                        0.004         0.022         1.0
          725     60                        0                 18      100         0              0                                        0.004         0.022         1.0
          726     60                        0                 18      100         0              0                                        0.004         0.022         1.0
          730     60                        0                 18      100         0              0                                        0.004         0.022         1.0
          769     60                        0                 18      100         0              0                                        0.004         0.022         1.0
          772     60                        0                 18      100         0              0                                        0.004         0.022         1.0
          779     60                        0                 18      100         0              0                                        0.004         0.022         1.0
