#include <iostream>
#include <fstream>
using namespace std;

#include <TCanvas.h>
#include <TGraphErrors.h>
#include <TAxis.h>
#include <TStyle.h>

int makeThresholdScanPlot(const char *fileName){

  ifstream file(fileName);
  if(!file){
    cout << __PRETTY_FUNCTION__ << ": ERROR!!! - cannot open file " << fileName << endl;
    return 1;
  }

  TGraphErrors *gr = new TGraphErrors();
  
  string line = "";
  while(getline(file, line)){
    double threshold = 0.;
    double calibratedThreshold = 0.;
    double efficiency = 0.;
    double efficiencyErr = 0.;
    sscanf(line.c_str(), "%lf\t%lf\t%lf\t%lf",
	   &threshold,
	   &calibratedThreshold,
	   &efficiency,
	   &efficiencyErr);
    gr -> SetPoint(gr -> GetN(), calibratedThreshold, 100. * efficiency);
    gr -> SetPointError(gr -> GetN()-1, 0, 100. * efficiencyErr);
  }
  
  file.close();

  TCanvas *cc = new TCanvas("cc", "cc", 0, 0, 1000, 1000);
  gr -> SetMarkerStyle(20);
  gr -> SetLineWidth(2);
  gr -> GetXaxis() -> SetTitle("threshold t #left[e^{-}#right]");
  gr -> GetYaxis() -> SetTitle("efficiency e [%]");
  gr -> Draw("ap");
  cc -> SaveAs("images/scan-W3R14.png");
  cc -> SaveAs("images/scan-W3R14.root");

  TGraphErrors *grDiff = new TGraphErrors();
  for(int i=1; i<gr -> GetN(); i++){
    double x1, x2, y1, y2;
    gr -> GetPoint(i-1, x1, y1);
    gr -> GetPoint(i, x2, y2);
    const double e1 = gr -> GetErrorY(i-1);
    const double e2 = gr -> GetErrorY(i);
    const double avX = (x2+x1) / 2.;
    const double diff = y1-y2;
    const double diffErr = sqrt(e1*e1+e2*e2);
    grDiff -> SetPoint(grDiff -> GetN(), avX, diff);
    grDiff -> SetPointError(grDiff -> GetN()-1, 0, diffErr);
  }
  
  TCanvas *ccDiff = new TCanvas("ccDiff", "ccDiff", 0, 0, 1000, 1000);
  grDiff -> SetMarkerStyle(20);
  grDiff -> SetLineWidth(2);
  grDiff -> GetXaxis() -> SetTitle("threshold t #left[e^{-}#right]");
  grDiff -> GetYaxis() -> SetTitle("#frac{de}{dt} #left[#frac{%}{e^{-}}#right]");
  grDiff -> Draw("ap");
  ccDiff -> SaveAs("images/scan-W3R14-diff.png");
  ccDiff -> SaveAs("images/scan-W3R14-diff.root");

  return 0;
}
