#include "TRandom3.h"

std::pair<Double_t,Double_t> getMedian(TRandom3 * rGen, std::vector<Double_t> xTracks, std::vector<Double_t> xTracks_Error){
  //bootstrap median of DUT hits, correct full data and fill tree
  //std::cout << "New split is full, calculating correction..." << std::endl;
  TH1D * medianSampleHisto = new TH1D("medians","Median Samples",50,1,0);
  medianSampleHisto->SetDefaultBufferSize(10001);
  Double_t median;
  Double_t medianSD;
  for(UInt_t j = 0; j < 10000; j++){
    std::vector<Double_t> sampleSet;
    for(UInt_t k = 0; k < xTracks.size(); k++){
      Int_t randomEntry = rGen->Rndm()*(xTracks.size()-1);
      Double_t value = rGen->Gaus(xTracks[randomEntry],xTracks_Error[randomEntry]); //add gaussian distributed factor of dX which is sigma of x to value
      sampleSet.push_back(value);
    }
    std::sort(sampleSet.begin(),sampleSet.end());
    if(sampleSet.size()%2 != 0){//sampleSet has odd number of values
      median = sampleSet[sampleSet.size()/2];
    }
    else if(sampleSet.size()%2 == 0){//sampleSet has even number of entries;
      median = (sampleSet[sampleSet.size()/2-1]+sampleSet[sampleSet.size()/2])/2;
    }
    medianSampleHisto->Fill(median);
  }
  medianSampleHisto->BufferEmpty();
  median = medianSampleHisto->GetMean();
  medianSD = medianSampleHisto->GetStdDev();
  //std::cout << "Calculated median and its error for current split: " << std::endl
  //          << "Median      : " << median << std::endl
  //          << "Median Error: " << medianSD << std::endl;
  std::pair<Double_t,Double_t> result;
  result.first = median;
  result.second = medianSD;
  delete medianSampleHisto;
  return result;
}
