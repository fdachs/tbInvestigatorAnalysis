#!/bin/bash
RUN=$1
IN=$(printf "/eos/atlas/atlascerngroupdisk/pixel-upgrade/cmos/TowerJazz/Investigator/Testbeam/Testbeam2017/cosmic_%06d/cosmic_%06d_000000.dat" $RUN $RUN)
OUT=$(printf "/afs/cern.ch/user/s/schioppa/testBeamAnalysis/raw/cosmic_%06d_000000" $RUN)
./tbConverter -c convertrcetoroot -f configs/RCE_6mod.cfg -i $IN -o ${OUT}.root