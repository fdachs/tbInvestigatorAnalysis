#include <string>
#include <vector>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <math.h>
#include <iostream>
#include <algorithm>
#include <regex>
#include <cstdint>
#include <stdint.h>
#include <ctime>

#include "TROOT.h"
#include "Rtypes.h"
#include "TFile.h"
#include "TDirectory.h"
#include "TTree.h"
#include "TBranch.h"

//#############################################################
//set up a struct with raw data containers and a method to fill
//processed data containers
//#############################################################
struct processedData{
  //processed data containers
  uint64_t unixTime;
  uint64_t microSecTime;
  float chVoltage[1024];
  float chTime[1024];
};
#pragma link C++ struct processedData+;

struct rawData{
  //raw data containers
  //global conainers for each board
  std::vector<int16_t> boardID;
  std::map<int16_t,std::vector<std::string>> channelID;
  std::map<int16_t,std::string> refChannel;
  std::map<int16_t,std::map<std::string,std::array<float,1024>>> channelTCalib;
  //event specific containers (overwritten for each event)
  int32_t eventID;
  int16_t timestamp[8];  //entry 0 is year, entry 5 is second, 6 is millisecond, 7 is microsecond (if it exitsts)
  int16_t range;
  std::map<int16_t,int16_t> boardTriggerCell;
  std::map<int16_t,std::map<std::string,int32_t>> channelScaler;
  std::map<int16_t,std::map<std::string,std::array<int16_t,1024>>> channelVoltage_raw;
  std::map<int16_t,std::map<std::string,processedData *>> procData;
  void processData(){
    std::tm timeSec = {timestamp[5],timestamp[4],timestamp[3],timestamp[2],timestamp[1],timestamp[0],0,0,0};
    //loop over boards
    for(auto board = boardID.begin(); board != boardID.end(); board++){
      //loop over channels
      for(auto channel = channelID[*board].begin(); channel != channelID[*board].end(); channel++){
        procData[*board][*channel]->unixTime = std::mktime(timeSec);
        procData[*board][*channel]->microSecTime = timestamp[6]*1000+timestamp[7]/10;
        for(int i = 0; i < 1024; i++){
          procData[*board][*channel]->chVoltage[i] = channelVoltage_raw[*board][*channel][i]/65536.0+range/1000.0-0.5;
          for(int j = 0, procdata[*board][*channel]->chTime[i] = 0; j < i; j++){
            procData[*board][*channel]->chTime[i] += channelTCalib[*board][*channel][(j+boardTriggerCell[*board])%1024];
          }
        }
      }
      //loop over channels again to align timestamps
      float t0 = procData[*board][refChannel[*board]]->chTime[(1024-boardTriggerCell[*board])%1024];
      for(auto channel = channelID[*board].begin(); channel != channelID[*board].end(); channel++){
        if(*channel != refChannel[*board]){
          float t1 = procData[*board][*channel]->chTime[(1024-boardTriggerCell[*board])%1024];
          for(int i = 0; i < 1024; i++){
            procData[*board][*channel]->chTime[i] += (t0-t1);
          }
        }
      }
    }
  };
};
#pragma link C++ struct rawData+;
#pragma link C++ class map<int16_t,map<string,processedData*>>+;


int drsreader(std::string inFilePath, std::string prefix, uint32_t maxEvents = 0){
  //############################################
  //read in arguments and set up root ouput file
  //############################################
  std::regex r("(.*\\/)*(.*)\\.(.+)$");
  std::smatch sm;
  std::regex_search(inFilePath,sm,r);
  if(sm.size() < 2){
    std::cout << "File path could not be read!" << std::endl;
    return 1;
  }
  else if(sm.size() > 1){
    std::cout << "Filename       : " << sm[sm.size()-2] << std::endl;
    std::cout << "Prefix         : " << prefix << std::endl;
  }

  std::string outFilePath = prefix + std::string(sm[sm.size()-2]) + ".root";
  TFile * DRSFile = new TFile(outFilePath.c_str(),"RECREATE");

  //#############
  //open raw file
  //#############
  FILE * inFile = fopen(inFilePath.c_str(),"r");
  if(inFile == NULL){
    std::cout << "Input file could not be opened!" << std::endl;
    return 1;
  }
  //containers for 4 and 2 byte words
  char c4[5];
  char c2[5];
  c4[4] = '\0';
  c2[2] = '\0';
  int readBytes = 0;

  //check file size and number of events in file
  fseek(inFile,0,SEEK_END);
  uint64_t fileSize = ftell(inFile);
  fseek(inFile,0,SEEK_SET);
  uint64_t headerSize = 0;
  uint64_t eventSize = 0;
  while(1){
    readBytes = fread(c4,4,1,inFile);
    headerSize += 4;
    if(strncmp(c4,"EHDR",4) == 0){
      break;
    }
    if(headerSize >= fileSize){
      std::cout << "No event headers found!" << std::endl;
      return 1;
    }
  }
  while(1){
    readBytes = fread(c4,4,1,inFile);
    eventSize += 4;
    if(strncmp(c4,"EHDR",4) == 0){
      break;
    }
    if(headerSize+eventSize >= fileSize){
      std::cout << "Event plus Header larger than file!" << std::endl;
      return 1;
    }
  }
  uint32_t nEvents = (fileSize-headerSize)/eventSize+1;
  std::cout << "File Size      : " << fileSize << std::endl
            << "Fileheader Size: " << headerSize << std::endl
            << "Event Size     : " << eventSize << std::endl
            << "# of Events    : " << nEvents << std::endl
            << std::endl           << std::endl;
  fseek(inFile,0,SEEK_SET);

  rawData * raw = rawData();





  return 0;
}
