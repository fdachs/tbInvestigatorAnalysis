#ifndef DRSREADER_H
#define DRSREADER_H 1

#include <string>
#include <vector>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <math.h>
#include <iostream>
#include <algorithm>
#include <regex>
#include <cstdint>
#include <time.h>

#include "TROOT.h"
#include "Rtypes.h"
#include "TFile.h"
#include "TDirectory.h"
#include "TTree.h"
#include "TBranch.h"

class DRSDataProcessor{
public:
  DRSDataProcessor();
  ~DRSDataProcessor();

  void SetTimeCalib(int16_t boardID, std::string channelID, std::array<float,1024> tC){
    if(timeCalibs[boardID].size() == 0){
      refChannels[boardID] = channelID;
    }
    timeCalibs[boardID][channelID] = tC;
  };
  uint64_t TimeToEpoch(uint16_t year, uint16_t month, uint16_t day, uint16_t hour,
                       uint16_t minute, uint16_t second){
    std::cout << "Polo!" << std::endl;
    struct tm * timeinfo = {0};
    timeinfo->tm_year  = year - 1900;
    timeinfo->tm_mon = month -1;
    timeinfo->tm_mday  = day;
    timeinfo->tm_hour  = hour -1;
    timeinfo->tm_min   = minute;
    timeinfo->tm_sec   = second;
    return mktime(timeinfo);
  };
  uint64_t TimeToMS(uint16_t millisecond, uint16_t microsecond){
    microsecond = (microsecond/10)*10; //do int division to reduce to 10us precision
    return microsecond+millisecond*1e3;
  };
  std::array<float,1024> ChannelTime(int16_t boardID, std::string channelID, int16_t triggerCell){
    std::array<float,1024> channelTime;
    float timeShift = timeCalibs[boardID][refChannels[boardID]][(1024-triggerCell)%1024]
                    - timeCalibs[boardID][channelID][(1024-triggerCell)%1024];
    float currentTime = 0.0;
    for(Int_t i = 0; i < 1024; i++){
      channelTime[i] = currentTime + timeShift;
      currentTime += timeCalibs[boardID][channelID][(i+triggerCell)%1024];
    }
    return channelTime;
  };
  std::array<float,1024> ChannelVoltage(std::array<int16_t,1024> rawVoltage, int16_t range){
    std::array<float,1024> channelVoltage;
    for(Int_t i = 0; i < 1024; i++){
      channelVoltage[i] = rawVoltage[i]/65536.0 + range/1000.0 - 0.5;
    }
    return channelVoltage;
  }

private:
  std::map<int16_t,std::map<std::string,std::array<float,1024>>> timeCalibs;
  std::map<int16_t,std::string> refChannels;
};

#endif
