#ifndef DRSANALYZER_H
#define DRSANALYZER_H 1

#include <string>
#include <vector>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <regex>
#include <cstdint>
#include <time.h>

#include "TROOT.h"
#include "TMath.h"
#include "Rtypes.h"
#include "TFile.h"
#include "TDirectory.h"
#include "TTree.h"
#include "TBranch.h"
#include "TF1.h"
#include "TFitResult.h"
#include "TGraph.h"
#include "TCanvas.h"
#include "TLine.h"

class CFGReader{
public:
  CFGReader(std::string cfgFile){
    m_cfgFile = std::ifstream(cfgFile);
    if(!m_cfgFile){
      std::cout << "Could not open config file!" << std::endl;
      m_cfgOpened = false;
    }
    else{
      m_cfgOpened = true;
      GetKeysAndValues();
    }
  };

  ~CFGReader(){};

  Bool_t FoundCFG(){
    if(m_cfgOpened){
      return true;
    }
    return false;
  };

  Float_t GetValue(std::string str){
    return m_keysAndValues[str];
  }

private:
  std::ifstream m_cfgFile;
  Bool_t m_cfgOpened;
  std::map<std::string,Float_t> m_keysAndValues;

  void GetKeysAndValues(){
    std::regex r("^(?!#)(\\w+)(\\s*\\=\\s*)((\\d+\\.\\d+)|\\d+)?");
    std::smatch sm;
    std::string line;
    while(std::getline(m_cfgFile,line)){
      std::regex_search(line,sm,r);
      if(sm.size() != 0){
        m_keysAndValues[std::string(sm[1])] = std::stof(std::string(sm[3]));
      }
    }
  }
};

class WaveFormFitter{
public:
  WaveFormFitter(CFGReader * cfgReader){
    m_cfgReader = cfgReader;
  };
  ~WaveFormFitter(){};

  void LinearFit(Float_t voltage[1024],Float_t time[1024],std::map<std::string,Float_t> & results){
    Int_t stepPos = 0.0;
    Float_t maxStep = 0.0;
    FindLikelyStep(voltage, time, maxStep, stepPos);
    if(maxStep > 0.03){//later put here some threshold based on noise
      TF1 * linearFit = new TF1("linearFit","[0]+[1]*0.5*(1+TMath::Erf(x-[2]))",time[0],time[1023]);
      Float_t start = 0.0;
      Float_t end   = 0.0;
      Float_t meanSize = m_cfgReader->GetValue("meanSize");
      for(Int_t i = 0; i < meanSize; i++){
        start += voltage[i];
        end   += voltage[1023-i];
      }
      start /= meanSize;
      end   /= meanSize;
      linearFit->SetParameters(start,maxStep,stepPos);
      std::cout << "maxStep: " << maxStep << ", stepPos: " << stepPos << std::endl;
      TGraph * pulse = new TGraph(1024,time,voltage);
      TCanvas * c = new TCanvas("linearFitCanvas","linearFitCanvas",-512,512,1024,1024);
      TFitResultPtr fitResult = pulse->Fit("linearFit","RS");
      //add results here
      results["chi2"] = fitResult->Chi2();
      results["amplitude"] = fitResult->Value(1);
      results["t0"] = fitResult->Value(2);
      results["offset"] = fitResult->Value(0);
      pulse->Draw();
      TLine * stepPosLine = new TLine(stepPos,start,stepPos,end);
      stepPosLine->SetLineColor(kRed);
      stepPosLine->Draw();
      c->WaitPrimitive();
      delete stepPosLine;
      delete linearFit;
      delete pulse;
      delete c;
    }
  };

  void LineFit(Float_t voltage[1024],Float_t time[1024],std::map<std::string,Float_t> & results){
    Float_t meanSize = m_cfgReader->GetValue("meanSize");
    TF1 * lineFit = new TF1("lineFit","[0]+[1]*x",time[0],time[1023]);
    Float_t start = 0.0;
    Float_t end   = 0.0;
    for(Int_t i = 0; i < meanSize; i++){
      start += voltage[i];
      end   += voltage[1023-i];
    }
    start /= meanSize;
    end   /= meanSize;
    lineFit->SetParameters(start,(end-start)/1024);
    TGraph * pulse = new TGraph(1024,time,voltage);
    TCanvas * c = new TCanvas("lineFitCanvas","lineFitCanvas",-512,512,1024,1024);
    TFitResultPtr fitResult = pulse->Fit("lineFit","RS");
    //add results here
    results["chi2"] = fitResult->Chi2();
    results["amplitude"] = fitResult->Value(1);
    results["t0"] = fitResult->Value(2);
    results["offset"] = fitResult->Value(0);
    pulse->Draw();
    c->WaitPrimitive();
    delete lineFit;
    delete pulse;
    delete c;
  };

  int PulseFit(Float_t voltage[1024],Float_t time[1024],std::map<std::string,Float_t> & results){
    TF1 * pulseFit = new TF1("pulseFit","[0]+0.5*x*[1]*(1+TMath::Erf([2]-x))+([3]*(1-exp(-(x-[2])/[4]))+[2]*[1])*0.5*(1+TMath::Erf(x-[2]))+0.5*(x-[2]-[4])*[5]*(1+TMath::Erf(x-[2]-[4]))",time[0],time[1023]);
    //pulseFit consists of several parts:
    //[0]: offset, active over whole range
    //[1]: slope before pulse, only active as long as x<[2] (= t0)
    //[2]: t0, slope ends here and exp step fit is "turned on", the whlole exp term should go quickly to 1 so it does not have to "turned off" again
    //[3]: amplitude
    //[4]: rise time
    //[5]: slope after pulse, i.e. "decay"
    //since there are 6 parameters, reasonable starting values will be important
    Float_t riseTimeLimit = m_cfgReader->GetValue("riseTimeLimit");
    Float_t start = 0.0;
    Float_t end   = 0.0;
    Float_t meanSize = m_cfgReader->GetValue("meanSize");
    for(Int_t i = 0; i < meanSize; i++){
      start += voltage[i];
      end   += voltage[1023-i];
    }
    start /= meanSize;
    end   /= meanSize;
    Int_t stepPos = 0.0;
    Float_t maxStep = 0.0;
    FindLikelyStep(voltage, time, maxStep, stepPos);
    if(maxStep < 0.01){
      return 1;
    }
    Int_t afterStepPos = stepPos;
    while(time[afterStepPos] < (time[stepPos]+riseTimeLimit)){
      afterStepPos++;
    }
    Float_t slopeGuess = (voltage[stepPos]-start)/time[stepPos];
    Float_t t0Guess = time[stepPos];
    Float_t riseTimeGuess = riseTimeLimit/2.0;
    Float_t decayGuess = (end-voltage[afterStepPos])/(time[1024]-time[afterStepPos]);
    pulseFit->SetParameters(start,slopeGuess,t0Guess,maxStep,riseTimeGuess,decayGuess);
    pulseFit->SetParLimits(1,-0.1,0.1);//maybe determine this with a slope scan with line fits!
    pulseFit->SetParLimits(2,time[stepPos]-riseTimeLimit,time[stepPos]+riseTimeLimit);//determine with linear fit scan? or just find likely step scan
    pulseFit->SetParLimits(3,0.5*maxStep,2*maxStep);//get lower limit from noise scan with line fits!
    pulseFit->SetParLimits(4,0,riseTimeLimit);//this is a value of experience...
    pulseFit->SetParLimits(5,-0.1,0.1);
    TGraph * pulse = new TGraph(1024,time,voltage);
    TCanvas * c = new TCanvas("pulseFitCanvas","pulseFitCanvas",-512,512,1024,1024);
    TFitResultPtr fitResult = pulse->Fit("pulseFit","RS");
    //add results here
    results["chi2"] = fitResult->Chi2();
    results["amplitude"] = fitResult->Value(1);
    results["t0"] = fitResult->Value(2);
    results["offset"] = fitResult->Value(0);
    pulse->Draw();
    TLine * stepPosLine = new TLine(time[stepPos],0,time[stepPos],1);
    stepPosLine->SetLineColor(kRed);
    stepPosLine->SetLineWidth(2);
    stepPosLine->Draw();
    c->BuildLegend(0.7,0.7,1.0,1.0,"Fit Parameters");
    c->WaitPrimitive();
    delete stepPosLine;
    delete pulseFit;
    delete pulse;
    delete c;
    return 0;
  };

private:
  CFGReader * m_cfgReader;

  void FindLikelyStep(Float_t voltage[1024], Float_t time[1024], Float_t & maxStep, Int_t & stepPos){
    Float_t riseTimeLimit = m_cfgReader->GetValue("riseTimeLimit");
    Float_t meanSize = m_cfgReader->GetValue("meanSize");
    for(Int_t i = 0; i < 1024; i++){
      Float_t plateau1 = 0.0;
      Float_t plateau2 = 0.0;
      Int_t j = i;
      //calculate first plateau
      while(j<i+meanSize){
        plateau1 += voltage[j];
        j++;
      }
      plateau1 /= meanSize;
      //go ahead in time by riseTimeLimit
      Int_t k = j;
      while(time[k]-time[j] < riseTimeLimit){
        k++;
      }
      //break if end of array is reached before plateau2 can be calculated
      if(k+meanSize > 1024){
        break;
      }
      //calculate plateau2
      Int_t l = k;
      while(l < k+meanSize){
        plateau2 += voltage[l];
        l++;
      }
      plateau2 /= meanSize;
      if(plateau2-plateau1 > maxStep){
        maxStep = plateau2-plateau1;
        //mark step position as end of plateau1
        stepPos = j;
      }
    }
  }

};
#endif
