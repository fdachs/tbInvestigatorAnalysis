#include "converter.h"
#include "analyzer.h"
#include "event.h"
#include "cfgreader.h"

#include "TROOT.h"
#include "TStyle.h"

#include <stdio.h>
#include <unistd.h>
#define GetCurrentDir getcwd

int main(int argc, char const *argv[]) {
  //############################################
  //read in data file and set up root ouput file
  //############################################
  //compile dictionary for output channel data format
  char buff[FILENAME_MAX];
  GetCurrentDir(buff,FILENAME_MAX);
  std::string macroPath = std::string(buff);
  //gROOT->SetMacroPath(macroPath.substr(0,macroPath.size()-6).c_str());
  //gROOT->ProcessLine(".L channelFormat.C++");

  std::regex r("(.*\\/)*(.*)\\.(.+)$");
  std::smatch sm;
  std::string filepath = argv[1];
  std::string prefix = argv[2];
  std::string cfgfile = argv[3];
  int32_t startEvent = std::stoi(std::string(argv[4]));
  int32_t endEvent = std::stoi(std::string(argv[5]));
  std::regex_search(filepath,sm,r);
  if(sm.size() < 2){
    std::cout << "File path could not be read!" << std::endl;
    return 1;
  }
  else if(sm.size() > 1){
    std::string fileName = sm[sm.size()-2];
    std::string fileType = sm[sm.size()-1];
    fileType = "." + fileType;
    if(fileType != ".dat"){
      std::cout << "Input file is no '.dat' file! Are you sure it is the right one?" << std::endl;
      std::cout << "Found file type:" << fileName.substr(fileName.size()-4,fileName.size()) << std::endl;
    }
    std::cout << "Filename       : " << sm[sm.size()-2] << std::endl;
    std::cout << "Prefix         : " << prefix << std::endl;
  }

  std::string outFilePath = prefix + std::string(sm[sm.size()-2]) + ".root";
  std::cout << "Output file will be placed into: " << outFilePath << std::endl;
  TFile * DRSFile = new TFile(outFilePath.c_str(),"RECREATE");
  TTree * DRSEventTree = new TTree("DRSEvent","Each event consists of all waveforms of all channels and boards found in the file.");

  DRSConverter * converter = new DRSConverter(filepath);
  CFGReader * cfgReader = new CFGReader(cfgfile);
  DRSAnalyzer * analyzer = new DRSAnalyzer(cfgReader);

  converter->Setup(DRSEventTree);
  if(endEvent == -1){
    endEvent = converter->GetNEvents();
  }
  TApplication eventDisplay("eventDisplay",0,NULL);
  for(int i = startEvent; i < endEvent; i++){
    DRSEvent * event = converter->ReadEvent(i);
    analyzer->AnalyzeEvent(event);
    converter->WriteEvent(event);
    event->Show();
  }
  std::cout << std::endl;
  DRSFile->Write();
  DRSFile->Close();
  std::cout << std::endl;
  std::cout << "Finished." << std::endl;
  delete converter;
  return 0;
}
