#ifndef ANALYZER_H
#define ANALYZER_H

#include <string>
#include <vector>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <regex>
#include <cstdint>
#include <time.h>

#include "TROOT.h"
#include "TMath.h"
#include "Rtypes.h"
#include "TFile.h"
#include "TDirectory.h"
#include "TTree.h"
#include "TBranch.h"
#include "TF1.h"
#include "TFitResult.h"
#include "TGraph.h"
#include "TCanvas.h"
#include "TLine.h"

#include "event.h"

class CFGReader;

class DRSAnalyzer{
public:
  DRSAnalyzer(CFGReader * cfg);
  ~DRSAnalyzer();

  void AnalyzeEvent(DRSEvent * event);

private:
    CFGReader * _cfgReader;
    void FindLikelyStep(float voltageData[1024], float timeData[1024],Double_t & maxStep,Int_t & stepPos);
    void LinearFit(float voltageData[1024],float timeData[1024],std::map<std::string,Double_t> & results);
    void PulseFit(float voltageData[1024],float timeData[1024],std::map<std::string,Double_t> & results);
};
#endif
