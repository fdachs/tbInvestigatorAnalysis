#ifndef EVENT_H
#define EVENT_H

#include <string>
#include <array>
#include <map>
#include <cstdint>
#include <vector>
#include <iostream>

#include "TROOT.h"
#include "TApplication.h"
#include "TStyle.h"
#include "TGraph.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "TF1.h"
#include "TLine.h"
#include "TLegend.h"

struct Channel{
  Double_t chi2;
  Double_t offset;
  Double_t slope;
  Double_t t0;
  Double_t amplitude;
  Double_t riseTime;
  Double_t decay;
  Double_t rawStep;
  Double_t rawT0;
  Double_t resRMS;
  Double_t resMean;
};

class DRSEvent{
public:
  DRSEvent(uint16_t eventID);
  ~DRSEvent();

  void Show();
  void SetVoltage(int16_t boardID, std::string channelID, std::array<float,1024> voltage);
  void SetTime(int16_t boardID, std::string channelID, std::array<float,1024> time);
  void SetTimeEpoch(uint64_t timeEpoch);
  void SetTimeMS(uint64_t timeMS);

  void AddBoard(int16_t boardID);
  void AddChannel(int16_t boardID, std::string channel);

  std::vector<int16_t> GetBoards();
  std::map<int16_t,std::vector<std::string>> GetChannels();

  uint16_t GetID(){return _eventID;};
  std::array<float,1024> GetVoltage(int16_t boardID, std::string channelID);
  std::array<float,1024> GetTime(int16_t boardID, std::string channelID);
  uint64_t GetTimeEpoch(){return _timeEpoch;};
  uint64_t GetTimeMS(){return _timeMS;};

  //stuff for analysis
  void SetFitParameters(int16_t boardID, std::string channelID, std::map<std::string,Double_t> paras);
  void SetFourierTransform(TH1D * ft_mag, TH1D * ft_pha);

  std::map<std::string,Double_t> GetFitParameters(int16_t boardID, std::string channelID);
  TH1D * GetFTMag();
  TH1D * GetFTPha();

private:
  uint16_t _eventID;
  TH1D * _ft_mag;
  TH1D * _ft_pha;
  std::vector<int16_t> _boardIDs;
  std::map<int16_t,std::map<std::string,std::map<std::string,Double_t>>> _fitParameters;
  std::map<int16_t,std::vector<std::string>> _channelIDs;
  std::map<int16_t,std::map<std::string,std::array<float,1024>>> _voltage;
  std::map<int16_t,std::map<std::string,std::array<float,1024>>> _time;
  uint64_t _timeEpoch;
  uint64_t _timeMS;
};
#endif
