#ifndef RAWDATACONVERTER_H
#define RAWDATACONVERTER_H

#include <string>
#include <vector>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <math.h>
#include <iostream>
#include <algorithm>
#include <regex>
#include <cstdint>
#include <time.h>

#include "event.h"

#include "TROOT.h"
#include "Rtypes.h"
#include "TFile.h"
#include "TDirectory.h"
#include "TTree.h"
#include "TBranch.h"

class DRSConverter{
public:
  DRSConverter(std::string filepath);
  ~DRSConverter();

  void Setup(TTree * DRSEventTree);
  int16_t GetNEvents();
  DRSEvent * ReadEvent(UInt_t eventID);
  void WriteEvent(DRSEvent * event);

  void SetTimeCalib(int16_t boardID, std::string channelID, std::array<float,1024> tC);
  uint64_t TimeToEpoch(uint16_t year, uint16_t month, uint16_t day, uint16_t hour,
                       uint16_t minute, uint16_t second);
  uint64_t TimeToMS(uint16_t millisecond, uint16_t microsecond);
  std::array<float,1024> ChannelTime(int16_t boardID, std::string channelID, int16_t triggerCell);
  std::array<float,1024> ChannelVoltage(std::array<uint16_t,1024> rawVoltage, int16_t range);

private:
  //input file infrastructure
  std::string _filepath;
  FILE * _inFile;
  int16_t _fileSize;
  int16_t _headerSize;
  int16_t _eventSize;
  int16_t _nEvents;
  std::map<int16_t,std::map<std::string,std::array<float,1024>>> _timeCalibs;
  std::map<int16_t,std::string> _refChannels;
  std::vector<int16_t> _boardIDs;
  std::map<int16_t,std::vector<std::string>> _channelIDs;
  //event data
  std::map<int16_t,std::map<std::string,std::array<float,1024>>> _channelTime;
  std::map<int16_t,std::map<std::string,std::array<float,1024>>> _channelVoltage;
  uint64_t _timeEpoch;
  uint64_t _timeMS;
  std::array<uint16_t,1024> _voltage;
  //output file infrastructure
  TTree * _DRSEventTree;
  std::map<int16_t,std::map<std::string,Channel>> _channelBranchLinks;
  //containers for timestamps
  uint16_t _eventYear;
  uint16_t _eventMonth;
  uint16_t _eventDay;
  uint16_t _eventHour;
  uint16_t _eventMinute;
  uint16_t _eventSecond;
  uint16_t _eventMillisecond;
  uint16_t _eventMicrosecond;
  //containers for event data
  int16_t  _eventRange;
  uint16_t _triggerCell;
  uint32_t _scaler;


};
#endif
