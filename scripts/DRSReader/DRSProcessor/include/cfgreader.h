#ifndef CFGREADER_H
#define CFGREADER_H

#include <string>
#include <fstream>

class CFGReader{
public:
  CFGReader(std::string cfgFile){
    m_cfgFile = std::ifstream(cfgFile);
    if(!m_cfgFile){
      std::cout << "Could not open config file!" << std::endl;
      m_cfgOpened = false;
    }
    else{
      m_cfgOpened = true;
      GetKeysAndValues();
    }
  };

  ~CFGReader(){};

  Bool_t FoundCFG(){
    if(m_cfgOpened){
      return true;
    }
    return false;
  };

  Float_t GetValue(std::string str){
    return m_keysAndValues[str];
  }

private:
  std::ifstream m_cfgFile;
  Bool_t m_cfgOpened;
  std::map<std::string,Float_t> m_keysAndValues;

  void GetKeysAndValues(){
    std::regex r("^(?!#)(\\w+)(\\s*\\=\\s*)((\\d+\\.\\d+)|\\d+)?");
    std::smatch sm;
    std::string line;
    while(std::getline(m_cfgFile,line)){
      std::regex_search(line,sm,r);
      if(sm.size() != 0){
        m_keysAndValues[std::string(sm[1])] = std::stof(std::string(sm[3]));
      }
    }
  }
};
#endif
