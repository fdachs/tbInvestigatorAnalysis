#include "event.h"

DRSEvent :: DRSEvent(uint16_t eventID){
  _eventID = eventID;
};

DRSEvent :: ~DRSEvent(){

};

void DRSEvent :: Show(){
  std::map<Int_t,TGraph *> waveforms;
  std::map<Int_t,TF1 *> fits;
  std::map<Int_t,TLegend *> legends;
  Int_t row = 0;
  Int_t col = 0;
  TCanvas * eventCanvas = new TCanvas("eventCanvas","eventCanvas",0,0,2048,1024);
  eventCanvas->Divide(4,_boardIDs.size());
  for(auto board = _boardIDs.begin(); board != _boardIDs.end(); board++){
    row = 0;
    for(auto channel = _channelIDs[*board].begin(); channel != _channelIDs[*board].end(); channel++){
      row++;
      Int_t canvasID = col*4+row;
      eventCanvas->cd(canvasID);
      waveforms[canvasID] = new TGraph(_voltage[*board][*channel].size(),
                                        _time[*board][*channel].data(),
                                        _voltage[*board][*channel].data());
      std::string title = "Event_" + std::to_string(_eventID) +
                          "_Bd_" + std::to_string(*board) +
                          "_Ch_" + *channel;
      waveforms[canvasID]->SetTitle(title.c_str());
      waveforms[canvasID]->GetXaxis()->SetRangeUser(0.0,_time[*board][*channel][1023]);
      waveforms[canvasID]->GetXaxis()->SetTitle("time [ns]");
      waveforms[canvasID]->GetXaxis()->SetTitleSize(0.06);
      waveforms[canvasID]->GetXaxis()->SetTitleOffset(1.4);
      waveforms[canvasID]->GetXaxis()->SetTickSize(0.04);
      waveforms[canvasID]->GetYaxis()->SetRangeUser(-0.1,1.1);
      waveforms[canvasID]->GetYaxis()->SetTitle("voltage [V]");
      waveforms[canvasID]->GetYaxis()->SetTitleSize(0.06);
      waveforms[canvasID]->GetYaxis()->SetTitleOffset(2.0);
      waveforms[canvasID]->GetYaxis()->SetTickSize(0.05);
      waveforms[canvasID]->Draw();
      if(_fitParameters[*board][*channel].size() != 0){
        //prepare fit function
        std::string fitname = "Event_" + std::to_string(_eventID) +
                              "_Bd_" + std::to_string(*board) +
                              "_Ch_" + (*channel) + "_fit";
        fits[canvasID] = new TF1(fitname.c_str(),
                                  "[0]+x*[1]*0.5*(1+TMath::Erf([2]-x)) + ([1]*[2]+[3]*(1-TMath::Exp(-(x-[2])/[4]))+(x-[2])*[5])*0.5*(1+TMath::Erf(x-[2]))",
                                  _time[*board][*channel][0],_time[*board][*channel][1023]);
        fits[canvasID]->SetParameter(0,_fitParameters[*board][*channel]["offset"]);
        fits[canvasID]->SetParameter(1,_fitParameters[*board][*channel]["slope"]);
        fits[canvasID]->SetParameter(2,_fitParameters[*board][*channel]["t0"]);
        fits[canvasID]->SetParameter(3,_fitParameters[*board][*channel]["amplitude"]);
        fits[canvasID]->SetParameter(4,_fitParameters[*board][*channel]["riseTime"]);
        fits[canvasID]->SetParameter(5,_fitParameters[*board][*channel]["decay"]);
        fits[canvasID]->SetLineColor(kRed);
        //prepare legend
        legends[canvasID] = new TLegend(0.5,0.3,0.9,0.9);
        legends[canvasID]->SetTextSize(0.03);
        for(auto para = _fitParameters[*board][*channel].begin(); para != _fitParameters[*board][*channel].end(); para++){
          std::string entry = para->first + ": " + std::to_string(para->second);
          legends[canvasID]->AddEntry((TObject*)NULL,entry.c_str(),"");
        }
        fits[canvasID]->Draw("same");
        legends[canvasID]->Draw("same");
        TLine * t0Line = new TLine(_fitParameters[*board][*channel]["rawT0"],0.0,
                                 _fitParameters[*board][*channel]["rawT0"],1.0);
        t0Line->SetLineColor(kGreen);
        t0Line->SetLineWidth(3);
        t0Line->Draw("same");
      }
    }
    col++;
  }
  eventCanvas->Update();
  eventCanvas->WaitPrimitive("eventCanvas.root");
  delete eventCanvas;
};

void DRSEvent :: SetVoltage(int16_t boardID, std::string channelID, std::array<float,1024> voltageData){
  _voltage[boardID][channelID] = voltageData;
};

void DRSEvent :: SetTime(int16_t boardID, std::string channelID, std::array<float,1024> timeData){
  _time[boardID][channelID] = timeData;
};

void DRSEvent :: SetTimeEpoch(uint64_t timeEpoch){
  _timeEpoch = timeEpoch;
};

void DRSEvent :: SetTimeMS(uint64_t timeMS){
  _timeMS = timeMS;
};

void DRSEvent :: AddBoard(int16_t boardID){
    _boardIDs.push_back(boardID);
};

void DRSEvent :: AddChannel(int16_t boardID, std::string channel){
  _channelIDs[boardID].push_back(channel);
};

std::vector<int16_t> DRSEvent :: GetBoards(){
  return _boardIDs;
};

std::map<int16_t,std::vector<std::string>> DRSEvent :: GetChannels(){
  return  _channelIDs;
};

std::array<float,1024> DRSEvent :: GetVoltage(int16_t boardID, std::string channelID){
  return _voltage[boardID][channelID];
};

std::array<float,1024> DRSEvent :: GetTime(int16_t boardID, std::string channelID){
  return _time[boardID][channelID];
};

//stuff for analysis
void DRSEvent :: SetFitParameters(int16_t boardID, std::string channelID, std::map<std::string,Double_t> paras){
  for(auto para = paras.begin(); para != paras.end(); para++){
    _fitParameters[boardID][channelID][para->first] = para->second;
  }
};

void DRSEvent :: SetFourierTransform(TH1D * ft_mag, TH1D * ft_pha){
  _ft_mag = ft_mag;
  _ft_pha = ft_pha;
};

std::map<std::string,Double_t> DRSEvent :: GetFitParameters(int16_t boardID, std::string channelID){
  return _fitParameters[boardID][channelID];
};

TH1D * DRSEvent :: GetFTMag(){
  return _ft_mag;
};

TH1D * DRSEvent :: GetFTPha(){
  return _ft_pha;
};
