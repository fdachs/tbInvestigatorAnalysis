#include "converter.h"
#include "event.h"

DRSConverter :: DRSConverter(std::string filepath){
  _filepath = filepath;
  _inFile = fopen(filepath.c_str(),"r");
  if(_inFile == NULL){
    std::cout << "Input file could not be opened!" << std::endl;
    abort();
  }
  _fileSize = -1;
  _headerSize = -1;
  _eventSize = -1;
  _nEvents = -1;
  _eventYear = 0;
  _eventMonth = 0;
  _eventDay = 0;
  _eventHour = 0;
  _eventMinute = 0;
  _eventSecond = 0;
  _eventMillisecond = 0;
  _eventMicrosecond = 0;
  _eventRange = -1;
  _triggerCell = 0;
  _scaler = 0;
};

DRSConverter :: ~DRSConverter(){
  fclose(_inFile);
};

void DRSConverter :: Setup(TTree * DRSEventTree){
  //store pointer to tree
  _DRSEventTree = DRSEventTree;
  //containers for 4 and 2 byte words
  char c4[5];
  char c2[3];
  c4[4] = '\0';
  c2[2] = '\0';
  int readBytes = 0;
  //check file size and number of events in file
  fseek(_inFile,0,SEEK_END);
  uint64_t fileSize = ftell(_inFile);
  fseek(_inFile,0,SEEK_SET);
  uint64_t headerSize = 0;
  uint64_t eventSize = 0;
  while(1){
    readBytes = fread(c4,4,1,_inFile);
    if(strncmp(c4,"EHDR",4) == 0){
      break;
    }
    if(headerSize >= fileSize){
      std::cout << "No event headers found!" << std::endl;
      abort();
    }
    headerSize += 4;
  }
  while(1){
    readBytes = fread(c4,4,1,_inFile);
    eventSize += 4;
    if(strncmp(c4,"EHDR",4) == 0){
      break;
    }
    if(headerSize+eventSize >= fileSize){
      std::cout << "Event plus Header larger than file!" << std::endl;
      abort();
    }
  }
  uint32_t nEvents = (fileSize-headerSize)/eventSize;
  std::cout << "File Size      : " << fileSize << std::endl
            << "Fileheader Size: " << headerSize << std::endl
            << "Event Size     : " << eventSize << std::endl
            << "# of Events    : " << nEvents << std::endl
            << std::endl           << std::endl;
  _fileSize = fileSize;
  _headerSize = headerSize;
  _eventSize = eventSize;
  _nEvents = nEvents;
  fseek(_inFile,0,SEEK_SET);
  readBytes = fread(c4,4,1,_inFile);
  if(strncmp("DRS2",c4,4) == 0){
    std::cout << "File header found: " << c4 << std::endl;
    readBytes = fread(c4,4,1,_inFile);
    if(strncmp("TIME",c4,4) == 0){
      std::cout << "Time header found: " << c4 << std::endl;
      //file header board loop
      readBytes = fread(c4,4,1,_inFile);
      while(c4[0] == 'B' && c4[1] == '#' && readBytes != 0){
        fseek(_inFile,-2,SEEK_CUR);
        int16_t bsn;
        readBytes = fread(&bsn,sizeof(bsn),1,_inFile);
        if(std::find(_boardIDs.begin(),_boardIDs.end(),bsn) == _boardIDs.end()){
          _boardIDs.push_back(bsn);
        }
        std::cout << "Board serial number found: " << bsn << std::endl;
        readBytes = fread(c4,4,1,_inFile);
        //channel loop
        while(c4[0] == 'C' && readBytes != 0){
          std::cout << "Time calibration of channel " << std::string(c4)
                    << " of board " << bsn << " found." << std::endl;
          std::string channelName = std::to_string(bsn) + "_" + std::string(c4);
          if(std::find(_channelIDs[bsn].begin(),_channelIDs[bsn].end(),channelName) == _channelIDs[bsn].end()){
            _channelIDs[bsn].push_back(std::string(c4));
          }
          std::array<float,1024> timeCalib;
          readBytes = fread(&timeCalib,sizeof(uint32_t),1024,_inFile);
          SetTimeCalib(bsn,std::string(c4),timeCalib);
          //set up branch for channel
          std::cout << "Creating branch " << channelName << " in output File Tree." << std::endl;
          _channelBranchLinks[bsn][std::string(c4)] = Channel();
          _DRSEventTree->Branch(channelName.c_str(),&_channelBranchLinks[bsn][std::string(c4)].chi2,"chi2/D:offset/D:slope/D:t0/D:amplitude/D:riseTime/D:decay/D:rawStep/D:rawT0/D:resRMS/D:resMean/D");
          //after the channel was read in, read next 4 bytes to see if there are more channels
          readBytes = fread(c4,4,1,_inFile);
        }
        std::cout << "Channels of board " << bsn << " read in." << std::endl;
      }
    }
    else{
      std::cout << "File seems to have no time header! Instead read: " << c4 << std::endl;
      abort();
    }
  }
  else{
    std::cout << "File seems to have no board header! Instead read: " << c4 << std::endl;
    abort();
  }
  std::cout << "Everything read in, setting back file pointer..." << std::endl;
  fseek(_inFile,0,SEEK_SET);
  std::cout << "done..." << std::endl;
};

void DRSConverter :: SetTimeCalib(int16_t boardID, std::string channelID, std::array<float,1024> timeCalib){
  if(_timeCalibs[boardID].size() == 0){
    _refChannels[boardID] = channelID;
  }
  _timeCalibs[boardID][channelID] = timeCalib;
};

uint64_t DRSConverter :: TimeToEpoch(uint16_t year, uint16_t month, uint16_t day,
                                         uint16_t hour, uint16_t minute, uint16_t second){
  struct tm timeinfo;
  timeinfo.tm_year = year-1900;
  timeinfo.tm_mon = month-1;
  timeinfo.tm_mday = day;
  timeinfo.tm_hour = hour-1;
  timeinfo.tm_min = minute;
  timeinfo.tm_sec = second;
  return mktime(&timeinfo);
};

int16_t DRSConverter :: GetNEvents(){
  return _nEvents;
}

uint64_t DRSConverter :: TimeToMS(uint16_t millisecond, uint16_t microsecond){
  //reduce to 10us resolution by integer division
  microsecond = (microsecond/10)*10;
  return microsecond+millisecond*1e3;
};

std::array<float,1024> DRSConverter :: ChannelTime(int16_t boardID, std::string channelID, int16_t triggerCell){
  std::array<float,1024> channelTime;
  float timeShift = _timeCalibs[boardID][_refChannels[boardID]][(1024-triggerCell)%1024]
                  - _timeCalibs[boardID][channelID][(1024-triggerCell)%1024];
  float currentTime = 0.0;
  for(Int_t i = 0; i < 1024; i++){
    channelTime[i] = currentTime + timeShift;
    currentTime += _timeCalibs[boardID][channelID][(i+triggerCell)%1024];
  }
  return channelTime;
};

std::array<float,1024> DRSConverter :: ChannelVoltage(std::array<uint16_t,1024> rawVoltage, int16_t range){
  std::array<float,1024> channelVoltage;
  for(Int_t i = 0; i < 1024; i++){
    channelVoltage[i] = rawVoltage[i]/65536.0 + range/1000.0 - 0.5;
  }
  return channelVoltage;
};

DRSEvent * DRSConverter :: ReadEvent(UInt_t eventID){
  DRSEvent * event = new DRSEvent(eventID);
  //containers for 4 and 2 byte words
  char c4[5];
  char c2[3];
  c4[4] = '\0';
  c2[2] = '\0';
  int readBytes = 0;
  uint32_t eventCounter = 0;
  //read event
  fseek(_inFile,_headerSize+eventID*_eventSize,SEEK_SET);
  readBytes = fread(c4,4,1,_inFile);
  if(strncmp("EHDR",c4,4) == 0 && readBytes != 0){
    readBytes = fread(&eventCounter,sizeof(eventCounter),1,_inFile);
    if(eventID == eventCounter-1){
      std::cout << "Found event " << eventID << ", reading in now...\r";
      fseek(_inFile,-(4+sizeof(eventCounter)),SEEK_CUR);
    }
    else{
      std::cout << std::endl;
      std::cout << "Jumped to event, but its ID doesn't match (wtf...):" << std::endl;
      abort();
    }
  }
  else{
    std::cout << std::endl;
    std::cout << "Got lost while jumping to event, check code." << std::endl;
    uint32_t fileSearch = 0;
    while(1){
      readBytes = fread(c4,4,1,_inFile);
      fileSearch += 4;
      if(strncmp(c4,"EHDR",4) == 0){
        readBytes = fread(&eventCounter,sizeof(eventCounter),1,_inFile);
        break;
      }
      if(fileSearch+_headerSize+eventID*_eventSize >= _fileSize){
        std::cout << std::endl;
        std::cout << "File search reached end of file!" << std::endl;
        abort();
      }
    }
    std::cout << std::endl;
    std::cout << "Found next event header after " << fileSearch << " bytes at event: " << eventID-1 << std::endl;
    abort();
  }
  readBytes = fread(c4,4,1,_inFile);
  //start reading event
  if(strncmp("EHDR",c4,4) == 0 && readBytes != 0){
    readBytes = fread(&eventCounter,sizeof(eventCounter),1,_inFile);
    //event in DRS data starts at 1 but this is inconvenient with most c++/ROOT codes...
    eventCounter -= 1;
    readBytes = fread(&_eventYear,sizeof(_eventYear),1,_inFile);
    readBytes = fread(&_eventMonth,sizeof(_eventMonth),1,_inFile);
    readBytes = fread(&_eventDay,sizeof(_eventDay),1,_inFile);
    readBytes = fread(&_eventHour,sizeof(_eventHour),1,_inFile);
    readBytes = fread(&_eventMinute,sizeof(_eventMinute),1,_inFile);
    readBytes = fread(&_eventSecond,sizeof(_eventSecond),1,_inFile);
    readBytes = fread(&_eventMillisecond,sizeof(_eventMillisecond),1,_inFile);
    //check if DRS file contains microsecond timestamps
    fseek(_inFile,2,SEEK_CUR);
    readBytes = fread(c2,2,1,_inFile);
    if(c2[0] == 'B' && c2[1] == '#'){
      _eventMicrosecond = 0;
      fseek(_inFile,-4,SEEK_CUR);
      readBytes = fread(&_eventRange,sizeof(_eventRange),1,_inFile);
    }
    else{
      fseek(_inFile,2,SEEK_CUR);
      readBytes = fread(c2,2,1,_inFile);
      if(c2[0] == 'B' && c2[1] == '#'){
        fseek(_inFile,-8,SEEK_CUR);
        readBytes = fread(&_eventMicrosecond,sizeof(_eventMicrosecond),1,_inFile);
        fseek(_inFile,2,SEEK_CUR);
        readBytes = fread(&_eventRange,sizeof(_eventRange),1,_inFile);
      }
      else{
        std::cout << std::endl;
        std::cout << "Error in event " << eventID << ": Board Header not found!" << std::endl;
        abort();
      }
    }
    //generate timestamps for root file
    event->SetTimeEpoch(TimeToEpoch(_eventYear, _eventMonth, _eventDay, _eventHour, _eventMinute, _eventSecond));
    event->SetTimeMS(TimeToMS(_eventMillisecond, _eventMicrosecond));
    //read in channel voltages and generate voltage and time arrays for root file
    readBytes = fread(c4,4,1,_inFile);
    //board loop
    while(c4[0] == 'B' && c4[1] == '#' && readBytes != 0){
      fseek(_inFile,-2,SEEK_CUR);
      int16_t bsn;
      readBytes = fread(&bsn,sizeof(bsn),1,_inFile);
      event->AddBoard(bsn);
      readBytes = fread(c2,2,1,_inFile);
      if(c2[0] != 'T' || c2[1] != '#'){
        std::cout << std::endl;
        std::cout << "Error in event " << eventID << ": Trigger Cell not found!" << std::endl;
        abort();
      }
      readBytes = fread(&_triggerCell,sizeof(_triggerCell),1,_inFile);
      readBytes = fread(c4,4,1,_inFile);
      //channel loop
      while(c4[0] == 'C' && readBytes != 0){
        readBytes = fread(&_scaler,sizeof(_scaler),1,_inFile);
        readBytes = fread(&_voltage,sizeof(uint16_t),1024,_inFile);
        event->AddChannel(bsn,std::string(c4));
        event->SetTime(bsn,std::string(c4),ChannelTime(bsn,std::string(c4),_triggerCell));
        event->SetVoltage(bsn,std::string(c4),ChannelVoltage(_voltage,_eventRange));
        readBytes = fread(c4,4,1,_inFile);
      }
    }
  }
  else{
    std::cout << std::endl;
    std::cout << "Could not find event header!" << std::endl;
    abort();
  }
  return event;
};
//for(auto u = _channelIDs[bsn].begin(); u != _channelIDs[bsn].end(); u++){
//  std::cout << *u << std::endl;
//}
void DRSConverter :: WriteEvent(DRSEvent * event){
  for(auto board = _boardIDs.begin(); board != _boardIDs.end(); ++board){
    for(auto channel = _channelIDs[*board].begin(); channel != _channelIDs[*board].end(); ++channel){
      std::map<std::string,Double_t> parameters = event->GetFitParameters(*board,*channel);
      _channelBranchLinks[*board][*channel].chi2 = parameters["chi2"];
      _channelBranchLinks[*board][*channel].offset = parameters["offset"];
      _channelBranchLinks[*board][*channel].slope = parameters["slope"];
      _channelBranchLinks[*board][*channel].t0 = parameters["t0"];
      _channelBranchLinks[*board][*channel].amplitude = parameters["amplitude"];
      _channelBranchLinks[*board][*channel].riseTime = parameters["riseTime"];
      _channelBranchLinks[*board][*channel].decay = parameters["decay"];
      _channelBranchLinks[*board][*channel].rawStep = parameters["rawStep"];
      _channelBranchLinks[*board][*channel].rawT0 = parameters["rawT0"];
      _channelBranchLinks[*board][*channel].resRMS = parameters["resRMS"];
      _channelBranchLinks[*board][*channel].resMean = parameters["resMean"];
    }
  }
  _DRSEventTree->Fill();
};
