#include "analyzer.h"
#include "event.h"
#include "cfgreader.h"

DRSAnalyzer :: DRSAnalyzer(CFGReader * cfgReader){
  _cfgReader = cfgReader;
};

DRSAnalyzer :: ~DRSAnalyzer(){};

void DRSAnalyzer :: AnalyzeEvent(DRSEvent * event){
  std::vector<int16_t> boards = event->GetBoards();
  std::map<int16_t,std::vector<std::string>> channels = event->GetChannels();
  for(auto board = boards.begin(); board != boards.end(); board++){
    for(auto channel = channels[*board].begin(); channel != channels[*board].end(); channel++){
      //fit with pulsefit or sth and store parameters
      std::array<float,1024> voltageData = event->GetVoltage(*board,*channel);
      std::array<float,1024> timeData = event->GetTime(*board,*channel);
      std::map<std::string,Double_t> results;
      PulseFit(&voltageData[0],&timeData[0],results);
      event->SetFitParameters(*board,*channel,results);
      //also do fourier stuff and store that
    }
  }
}

void DRSAnalyzer :: LinearFit(float voltageData[1024],float timeData[1024],std::map<std::string,Double_t> & results){
  Double_t meanSize = _cfgReader->GetValue("meanSize");
  TF1 * lineFit = new TF1("lineFit","[0]+[1]*x",timeData[0],timeData[1023]);
  Double_t start = 0.0;
  Double_t end   = 0.0;
  for(Int_t i = 0; i < meanSize; i++){
    start += voltageData[i];
    end   += voltageData[1023-i];
  }
  start /= meanSize;
  end   /= meanSize;
  lineFit->SetParameters(start,(end-start)/1024);
  TGraph * pulse = new TGraph(1024,timeData,voltageData);
  TFitResultPtr fitResult = pulse->Fit("lineFit","RSQ");
  Double_t mean = 0.0;
  Double_t rms = 0.0;
  for(auto i = 0; i < 1024; i++){
    Double_t res = lineFit->Eval(timeData[i])-pulse->Eval(timeData[i]);
    mean += res;
    rms += std::pow(res,2);
  }
  mean/=1024;
  rms/=1024;
  //add results here
  results["chi2"] = fitResult->Chi2();
  results["offset"] = fitResult->Value(0);
  results["slope"] = fitResult->Value(1);
  results["resMean"] = mean;
  results["resRMS"] = rms;
  delete lineFit;
  delete pulse;
};

void DRSAnalyzer :: PulseFit(float voltageData[1024],float timeData[1024],std::map<std::string,Double_t> & results){
  //TF1 * pulseFit = new TF1("pulseFit","[0]+0.5*x*[1]*(1+TMath::Erf([2]-x))+([3]*(1-exp(-(x-[2])/[4]))+[2]*[1])*0.5*(1+TMath::Erf(x-[2]))+0.5*(x-[2]-[4])*[5]*(1+TMath::Erf(x-[2]-[4]))",timeData[0],timeData[1023]);
  //pulseFit consists of several parts:
  //[0]: offset, active over whole range
  //[1]: slope before pulse, only active as long as x<[2] (= t0)
  //[2]: t0, slope ends here and exp step fit is "turned on", the whlole exp term should go quickly to 1 so it does not have to "turned off" again
  //[3]: amplitude
  //[4]: rise timeData
  //[5]: slope after pulse, i.e. "decay"
  //since there are 6 parameters, reasonable starting values will be important
  Double_t riseTimeLimit = _cfgReader->GetValue("riseTimeLimit");
  Double_t start = 0.0;
  Double_t end   = 0.0;
  Double_t meanSize = _cfgReader->GetValue("meanSize");
  for(Int_t i = 0; i < meanSize; i++){
    start += voltageData[i];
    end   += voltageData[1023-i];
  }
  start /= meanSize;
  end   /= meanSize;
  Int_t stepPos = 0.0;
  Double_t maxStep = 0.0;
  FindLikelyStep(voltageData, timeData, maxStep, stepPos);
  Int_t afterStepPos = stepPos;
  while(timeData[afterStepPos] < (timeData[stepPos]+riseTimeLimit)){
    afterStepPos++;
  }
  //avoid division by 0
  int i = 0;
  while(timeData[stepPos]-timeData[i] == 0.0){
    i++;
  }
  stepPos += i;
  i = 0;
  while(timeData[afterStepPos]-timeData[1023-i] == 0.0){
    i++;
  }
  //calculate guesses for parameters to initilaize fit
  Double_t slopeGuess = (voltageData[stepPos]-start)/(timeData[stepPos]-timeData[0]);
  Double_t t0Guess = timeData[stepPos];
  Double_t riseTimeGuess = riseTimeLimit/2.0;
  Double_t decayGuess = (end-voltageData[afterStepPos])/(timeData[1023-i]-timeData[afterStepPos]);
  TF1 * pulseFit2 = new TF1("pulseFit2","[0]+x*[1]*0.5*(1+TMath::Erf([2]-x)) + ([1]*[2]+[3]*(1-TMath::Exp(-(x-[2])/[4]))+(x-[2])*[5])*0.5*(1+TMath::Erf(x-[2]))",timeData[stepPos]-100.0,timeData[stepPos]+100.0);
  pulseFit2->SetParameters(start,slopeGuess,t0Guess,maxStep,riseTimeGuess,decayGuess);
  pulseFit2->SetParLimits(1,slopeGuess*0.8,slopeGuess*1.2);//maybe determine this with a slope scan with line fits!
  pulseFit2->SetParLimits(2,timeData[stepPos]-riseTimeLimit,timeData[stepPos]+riseTimeLimit);//determine with linear fit scan? or just find likely step scan
  pulseFit2->SetParLimits(3,0.5*maxStep,2*maxStep);//get lower limit from noise scan with line fits!
  pulseFit2->SetParLimits(4,0,riseTimeLimit*2);//this is a value of experience...
  pulseFit2->SetParLimits(5,decayGuess*0.8,decayGuess*1.2);
  TGraph * pulse = new TGraph(1024,timeData,voltageData);
  TFitResultPtr fitResult = pulse->Fit("pulseFit2","SRQ");
  Double_t mean = 0.0;
  Double_t rms = 0.0;
  for(auto i = 0; i < 1024; i++){
    Double_t res = pulseFit2->Eval(timeData[i])-pulse->Eval(timeData[i]);
    mean += res;
    rms += std::pow(res,2);
  }
  mean/=1024;
  rms =std::sqrt(rms/1024);
  //add results here
  results["chi2"] = fitResult->Chi2();
  results["offset"] = fitResult->Value(0);
  results["slope"] = fitResult->Value(1);
  results["t0"] = fitResult->Value(2);
  results["amplitude"] = fitResult->Value(3);
  results["riseTime"] = fitResult->Value(4);
  results["decay"] = fitResult->Value(5);
  results["rawStep"] = maxStep;
  results["rawT0"] = timeData[stepPos];
  results["resRMS"] = rms;
  results["resMean"] = mean;
  delete pulseFit2;
  delete pulse;
};

void DRSAnalyzer :: FindLikelyStep(float voltageData[1024], float timeData[1024], Double_t & maxStep, Int_t & stepPos){
  Double_t riseTimeLimit = _cfgReader->GetValue("riseTimeLimit");
  Double_t meanSize = _cfgReader->GetValue("meanSize");
  for(Int_t i = 0; i < 1024; i++){
    Double_t plateau1 = 0.0;
    Double_t plateau2 = 0.0;
    Int_t j = i;
    //calculate first plateau
    while(j<i+meanSize){
      plateau1 += voltageData[j];
      j++;
    }
    plateau1 /= meanSize;
    //go ahead in timeData by riseTimeLimit
    Int_t k = j;
    while(timeData[k]-timeData[j] < riseTimeLimit){
      k++;
    }
    //break if end of array is reached before plateau2 can be calculated
    if(k+meanSize > 1024){
      break;
    }
    //calculate plateau2
    Int_t l = k;
    while(l < k+meanSize){
      plateau2 += voltageData[l];
      l++;
    }
    plateau2 /= meanSize;
    if(plateau2-plateau1 > maxStep){
      maxStep = plateau2-plateau1;
      //mark step position as end of plateau1
      stepPos = j;
    }
  }
};
