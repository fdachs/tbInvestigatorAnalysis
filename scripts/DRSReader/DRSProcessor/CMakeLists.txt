#Minimum CMake Version
cmake_minimum_required(VERSION 3.0 FATAL_ERROR)

#set(setup "source /cvmfs/sft.cern.ch/lcg/views/LCG_89/x86_64-slc6-gcc62-opt/setup.sh")
#execute_process(COMMAND ${setup})

#project name
project(DRDProcessor)

#output
set(CMAKE_BINARY_DIR ${CMAKE_SOURCE_DIR})
set(LIBRARY_OUTPUT_PATH ${CMAKE_BINARY_DIR}/lib)
set(EXECUTABLE_OUPUT_PATH ${CMAKE_BINARY_DIR}/bin)

#ROOT
find_package(ROOT REQUIRED)
include(${ROOT_USE_FILE})

#include folder
include_directories("${PROJECT_SOURCE_DIR}/include")

#locate sources and headers
file(GLOB sources ${PROJECT_SOURCE_DIR}/src/*.cpp)
file(GLOB headers ${PROJECT_SOURCE_DIR}/include/*.h)

add_executable(DRSProcessor DRSProcessor.cpp ${sources} ${headers})
target_link_libraries(DRSProcessor ${ROOT_LIBRARIES})
